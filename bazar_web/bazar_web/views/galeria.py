from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render

def galeria_index(request):
    print('Retornar pagina galeria.html')
    hola = saludo()
    return render(request, 'galeria.html', {"name":(f"{hola} Mauricio")})

def saludo():
    return "Hola, cómo estas"